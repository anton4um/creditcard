import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidationErrors
} from "@angular/forms";
import { Observable, fromEvent, Subscription, BehaviorSubject } from "rxjs";
import { take } from "rxjs/operators";
import { resolve } from "url";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("cardNumber", { read: ElementRef, static: true })
  cardNumberEl: ElementRef;

  @ViewChild("expiryDate", { read: ElementRef, static: true })
  cardExpiryDateEl: ElementRef;

  @ViewChild("cvcInput", { read: ElementRef, static: true })
  cardCvcInputEl: ElementRef;

  expiryDateControlErrorsObs: BehaviorSubject<any>;
  lastNameErrorsObs: BehaviorSubject<any>;

  cardNumChangeEvent: Observable<any>;
  cardExpiryChangeEvent: Observable<any>;
  cardCvcChangeEvent: any;

  cardForm: FormGroup;

  cardNumSub: Subscription;
  cardExpDateSub: Subscription;
  cvcSub: Subscription;

  constructor() {}
  expiryErrorMessage: string = "";
  hintMessage: string = "";
  namesErrorMessage: string = "";

  maxExpiryDate = new Date(new Date().getFullYear() + 10, 11);
  minExpiryDate = new Date(new Date().getFullYear(), new Date().getMonth());
  ngOnInit() {
    this.cardForm = new FormGroup({
      number: new FormControl(
        null,
        [Validators.required],
        this.cardNumberValidatorAsync.bind(this)
      ),
      firstName: new FormControl(null, [
        Validators.required,
        // Validators.pattern("[A-Za-z]{3,}"),
        this.namesValueLengthValidatorSync.bind(this),
        this.namesForbiddenSymbolsValidatorSync
      ]),
      lastName: new FormControl(null, [
        Validators.required,
        // Validators.pattern("[A-Za-z]{3,}"),
        this.namesValueLengthValidatorSync.bind(this),
        this.namesForbiddenSymbolsValidatorSync
      ]),
      cvc: new FormControl(
        null,
        [Validators.required, Validators.pattern("[0-9]{3,}")],
        this.cardCvcValidatorAsync.bind(this)
      ),
      expiry: new FormControl(
        null,
        [Validators.required],
        this.cardExpirationDateValidatorAsyc.bind(this)
      )
    });
  }
  timerId;
  ngAfterViewInit() {
    this.cardNumChangeEvent = fromEvent(
      this.cardNumberEl.nativeElement,
      "change"
    );
    this.cardExpiryChangeEvent = fromEvent(
      this.cardExpiryDateEl.nativeElement,
      "change"
    );
    this.cardCvcChangeEvent = fromEvent(
      this.cardCvcInputEl.nativeElement,
      "change"
    );

    this.cardForm.get("lastName").valueChanges.subscribe(value => {
      let errors = this.cardForm.get("lastName").errors;
      console.log(
        "\nLast Name value changes: \n",
        value,
        "\n Last Name errors: \n",
        errors
      );
      this.cardErrorHandler({ ...errors });
    });
    this.cardExpiryChangeEvent.subscribe(event => {
      // console.log("\nSecond Subscriber event fired: \n", event);
      let controlErrors: any;
      setTimeout(() => {
        controlErrors = this.cardForm.get("expiry").errors;
        // console.log("\ncontrol Err: \n", controlErrors);
        this.cardErrorHandler({
          ...controlErrors
        });
      }, 200);
    });
  }

  cardNumberValidatorAsync(
    control: FormControl
  ): Promise<any> | Observable<any> {
    let promise = new Promise<any>((resolve, reject) => {
      this.cardNumSub = this.cardNumChangeEvent
        .pipe(take(1))
        .subscribe(event => {
          // console.log("event fired: \n", event);
          if (event.target.classList.contains("jp-card-invalid")) {
            resolve({ cardNumberIsInvalid: true });
          } else {
            resolve(null);
          }
        });
    });

    return promise;
  }
  cardExpirationDateValidatorAsyc(
    control: FormControl
  ): Promise<any> | Observable<any> {
    let promise: Promise<any>;
    promise = new Promise<any>((resolve, reject) => {
      this.cardExpDateSub = this.cardExpiryChangeEvent
        .pipe(take(1))
        .subscribe(event => {
          let parsedYear: any;
          let parsedMonth: any;
          let enteredExpiryDate: Date;

          // console.log("\ncontrol value length: \n", control.value.length);
          // if (control.value.length === 6) {
          //   parsedMonth = control.value.replace(/\s+/g, "").slice(0, 2) - 1;
          //   // console.log("\nParsed Values parsedMonth: \n", parsedMonth);
          //   if (parsedMonth < 0 || parsedMonth > 11) {
          //     // console.log("\nparsedMonth is Invalid: \n", parsedMonth);
          //     resolve({ cardExpiryMonthIsInvalid: true });
          //   } else {
          //     // console.log("\nparsedMonth is Valid: \n", parsedMonth);
          //     this.cardErrorHandler({ cardExpiryMonthIsValid: true });
          //   }
          // }
          // if (control.value.length === 7) {
          parsedMonth = control.value.replace(/\s+/g, "").slice(0, 2) - 1;
          parsedYear = "20" + control.value.replace(/\s+/g, "").slice(-2);
          enteredExpiryDate = new Date(+parsedYear, +parsedMonth);
          // console.log("\nenteredExpiryDate: \n", enteredExpiryDate);
          if (
            isNaN(enteredExpiryDate.getTime()) ||
            enteredExpiryDate.getFullYear() < 2000
          ) {
            resolve({ cardExpiryDateFormatIsInvalid: true });
            this.cardErrorHandler({ cardExpiryDateFormatIsInvalid: true });
          } else {
            if (enteredExpiryDate < this.minExpiryDate) {
              resolve({ cardHasExpired: true });
              this.cardErrorHandler({ cardHasExpired: true });
            } else if (+enteredExpiryDate > +this.maxExpiryDate) {
              resolve({ cardExpiryDateIsExceed: true });
              this.cardErrorHandler({ cardExpiryDateIsExceed: true });
            } else if (event.target.classList.contains("jp-card-invalid")) {
              resolve({ cardExpiryDateIsInvalid: true });
              this.cardErrorHandler({ cardExpiryDateIsInvalid: true });
            } else {
              this.cardErrorHandler({ cardExpiryDateIsValid: true });
              resolve(null);
            }
          }
          // }
          // }
        });
    });
    return promise;
  }

  cardErrorHandler(errorsListObj) {
    for (let key in errorsListObj) {
      // console.log("\nError Key In ErrorHendler: \n", key);
      switch (key) {
        // for card hint messages START SECTION
        case "cardExpiryMonthIsValid":
          this.hintMessage = "Expiry Date Month Is Valid";
          break;
        case "cardExpiryDateIsValid":
          this.hintMessage = "Expiry Date Is Valid";
          break;
        // for card hint messages END SECTION

        // for names controls,look at the top START SECTION
        case "lengthLessMinimal":
          this.namesErrorMessage = "Invalid, Value Length Less Than 3 Chars";
          break;
        case "lengthExcessMaximal":
          this.namesErrorMessage = "Invalid, Value Length Excess 30 Chars";
          break;
        // for names controls END SECTION

        //card Expiry Date START SECIOTN
        case "cardHasExpired":
          this.expiryErrorMessage =
            "Invalid, Entered Date Is Less Than Current Date";
          break;
        case "cardExpiryDateIsExceed":
          this.expiryErrorMessage =
            "Invalid, Entered Date Is Greater Than Max Date Expiry";
          break;
        case "cardExpiryYearIsInvalid":
          this.expiryErrorMessage = "Expiry Year Is Invalid";
          break;
        case "cardExpiryMonthIsInvalid":
          this.expiryErrorMessage = "Expiry Month Is Invalid: Must be 01 to 12";
          break;
        case "cardExpiryDateIsInvalid":
          this.expiryErrorMessage = "Expiry Date Is Invalid, Verify Month Or Year";
          break;
        case "cardExpiryDateFormatIsInvalid":
          this.expiryErrorMessage = "Expiry Date Format Is Invalid, Verify MM/YY";
          break;
        //card Expiry Date END SECIOTN
        default:
          return null;
      }
    }
  }

  cardCvcValidatorAsync(control: FormControl): Promise<any> | Observable<any> {
    let promise = new Promise<any>((resolve, reject) => {
      this.cvcSub = this.cardCvcChangeEvent.pipe(take(1)).subscribe(event => {
        if (event.target.classList.contains("jp-card-invalid")) {
          resolve({ cvcFieldIsInvalid: true });
        } else {
          resolve(null);
        }
      });
    });
    return promise;
  }

  namesForbiddenSymbolsValidatorSync(
    control: FormControl
  ): { [s: string]: boolean } {
    if (control.value != null) {
      if (control.value.match(/[^A-Za-z]/g)) {
        return { forbiddenSymbolsIsPresent: true };
      }
    } else {
      return null;
    }
  }

  namesValueLengthValidatorSync(
    control: FormControl
  ): { [s: string]: boolean } {
    if (control.value) {
      if (control.value.length < 3) {
        control.setErrors({ lengthLessMinimal: true });
        this.cardErrorHandler({ lengthLessMinimal: true });
        return { lengthLessMinimal: true };
      }
      if (control.value.length > 30) {
        control.setErrors({ lengthExcessMaximal: true });
        this.cardErrorHandler({ lengthExcessMaximal: true });
        return { lengthExcessMaximal: true };
      }
      return null;
    }
  }

  onSubmit() {
    console.log("\nForm Group: \n", this.cardForm);
  }

  ngOnDestroy() {
    this.cardNumSub.unsubscribe();
    this.cvcSub.unsubscribe();
    this.cardExpDateSub.unsubscribe();
  }
}
