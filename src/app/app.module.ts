import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {FormsModule} from "@angular/forms";

import { AppComponent } from "./app.component";
import { CardModule } from "ngx-card/ngx-card";

import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { BillingComponent } from './billing/billing.component';


@NgModule({
  declarations: [AppComponent, BillingComponent],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    CardModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
